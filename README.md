<p align="center">
  <img alt="Letmeask" src=".github/logo.svg" width="160px">
</p>

 <p align="center">
  <img src="https://img.shields.io/static/v1?label=NLW&message=06&color=8257E5&labelColor=000000" alt="NLW Together 06" />

  <img  src="https://img.shields.io/static/v1?label=license&message=MIT&color=8257E5&labelColor=000000" alt="License">   
</p>

<h1 align="center">
  <p align="left">Desktop:</p>
  <img alt="Letmeask" src=".github/cover.png" />
</h1>

<h1 align="center">
  <p align="left">Mobile:</p>
  <img alt="Letmeask" src=".github/cover-mobile.png" />
</h1>
  


## Próximo nível
<ul>
  <li>[x] Responsividade</li>
  <li>[x] Tema dark</li>
  <li>[x] PWA</li>
  <li>[ ] Github</li>
 </ul>
