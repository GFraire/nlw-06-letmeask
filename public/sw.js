const CACHE_NAME = 'letmeask-app'

const urlsToCache = [
    '/',
    '/src/styles/global.scss',
    '/src/index.tsx'
]

this.addEventListener('install', event => {
    this.skipWaiting()

    event.waitUntil(
      caches.open(CACHE_NAME)
      .then(cache => {
          console.log('Opened cache')
          return cache.addAll(urlsToCache)
      })
    )
})

this.addEventListener('activate', event => {
  // event.waitUntil(this.clients.claim())
  event.waitUntil(caches.keys().then(cacheNames => {
    return Promise.all(cacheNames.filter(cacheName => {
      return cacheName.startsWith('letmeask-app')
    })).filter(cacheName => (cacheName !== CACHE_NAME)).map(cacheName => {
      return caches.delete(cacheName)
    })
  }))
})

this.addEventListener('fetch', function  (event) {
    event.respondWith(
      caches.match(event.request)
      .then(response => {
          return response || fetch(event.request);
      })
      .catch(() => {
        console.log("error fetch")
      })
  );
});  